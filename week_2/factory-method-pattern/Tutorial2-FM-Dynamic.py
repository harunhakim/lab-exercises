#	Mandatory 3: Create a program so it implement Factory Method Pattern. There are a template class to use Factory Method Pattern but you are free to explore your idea in implementing Factory Method Pattern.

def main():
        sisi = int(input("Masukan angka: "))
        board = Boardnxn(sisi, sisi)
        print (board)

class AbstractBoard:
	
	def __init__(self, rows, columns):
		self.board = [[None for _ in range(columns)] for _ in range(rows)]
		self.populate_board(rows, columns)
		
	def populate_board(self, rows, columns):
		raise NotImplementedError()

	def __str__(self):
		squares = []
		for x, row in enumerate(self.board):
			for y, column in enumerate(self.board):
				squares.append(self.board[x][y])
			squares.append("\n")
		return "".join(squares)

class Boardnxn(AbstractBoard):
	def __init__(self, rows, columns):
		super().__init__(rows, columns)
		
	def populate_board(self, rows, columns):
		for row in range(rows):
			for column in range(columns):
				if (row % 2 and column % 2): self.board[row][column] = V()
				elif (row % 2 and not (column%2)): self.board[row][column] = U()
				elif (not(row % 2) and column%2): self.board[row][column] = Circle()
				else: self.board[row][column] = Cross()

# Mandatory 3: Uncomment the codes below
class Piece(str):
	__slots__ = ()
	
class Circle(Piece):
# Mandatory 3: Implement the Factory Method in here
        __slots__ = ()

        def __new__(Class):
                return super().__new__(Class,"o")

class Cross(Piece):
# Mandatory 3: Implement the Factory Method in here
        __slots__ = ()

        def __new__(Class):
                return super().__new__(Class,"x")
				
class U(Piece):
# Mandatory 3: Implement the Factory Method in here
        __slots__ = ()

        def __new__(Class):
                return super().__new__(Class,"u")
				
class V(Piece):
# Mandatory 3: Implement the Factory Method in here
        __slots__ = ()

        def __new__(Class):
                return super().__new__(Class,"v")

if __name__ == "__main__":
    main()
				
