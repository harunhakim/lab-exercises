class ProcessHandler(object):

    def __init__(self, successor=None, name=None):
        self._name = name
        self._successor = successor

    def has_successor(self):
        return self._successor is not None

    def process(self):
        pass

    def give_process(self, data):
        self._successor.process(data)


class ProcessLevel1(ProcessHandler):

    def __init__(self, s, n):
        super(ProcessLevel1, self).__init__(s, n)

    def process(self, data):
        # TODO Implement me!

        if (data <= 19 and data >= 0):
            print("Using {}\nProcessing data {}".format(self._name,data)) 
        else:
            self.give_process(data)
        
        pass


class ProcessLevel2(ProcessHandler):

    def __init__(self, s, n):
        # TODO Implement me!

        super(ProcessLevel2, self).__init__(s, n)
        
        pass

    def process(self, data):
        # TODO Implement me!

        if (data <= 39 and data >= 20):
            print("Using {}\nProcessing data {}".format(self._name,data)) 
        else:
            self.give_process(data) 
        
        pass


class ProcessLevel3(ProcessHandler):

    def __init__(self, s, n):
        # TODO Implement me!

        super(ProcessLevel3, self).__init__(s, n)
        
        pass

    def process(self, data):
        # TODO Implement me!

        if (data <= 59 and data >= 40):
             print("Using {}\nProcessing data {}".format(self._name,data)) 
        else:
            print("This data can't be processed") 
        
        pass


def main():
    p3 = ProcessLevel3(s=None, n='Process Level 3')
    p2 = ProcessLevel2(s=p3, n='Process Level 2')
    p1 = ProcessLevel1(s=p2, n='Process Level 1')
    p1.process(70)


if __name__ == "__main__":
    main()
