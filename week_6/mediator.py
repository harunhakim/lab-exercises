import random
import time


class TestController:

    def __init__(self):
        self._testmanager = None
        self._bProblem = 0

    def setup(self):
        print("Setting up the Test")
        time.sleep(0.1)
        self._testmanager.prepareReporting()

    def execute(self):
        # TODO COMPLETE ME

        if not self._bProblem:
            print ("Executing the test")
            time.sleep(0.1)
        else:
            print ("Problem in setup. Test not executed.")
        
        pass

    def tearDown(self):
        # TODO COMPLETE ME

        if not self._bProblem:
            print ("Tearing down")
            time.sleep(0.1)
            self._testmanager.publishReport()
        else:
            print ("Test not executed. No tear down required.")
        
        pass

    def setTM(self, testmanager):
        # TODO COMPLETE ME

        self._testmanager = testmanager
        
        pass

    def setProblem(self, value):
        # TODO COMPLETE ME

        self._bProblem = value
        
        pass


class Reporter:

    def __init__(self):
        self._testmanager = None

    def prepare(self):
        print("Reporter Class is preparing to report the results")
        time.sleep(0.1)

    def report(self):
        print("Reporting the results of Test")
        time.sleep(0.1)

    def setTM(self, testmanager):
        self._testmanager = testmanager


class Database:

    def __init__(self):
        self._testmanager = None

    def insert(self):
        print("Inserting the execution begin status in the Database ")
        time.sleep(0.1)
        if random.randrange(1, 4) == 3:
            return -1

    def update(self):
        print("Updating the test results in Database")
        time.sleep(0.1)

    def setTM(self, testmanager):
        self._testmanager = testmanager


class TestManager:

    def __init__(self):
        # TODO Complete Me

        self._reporter = None
        self._database = None
        self._testcontroller = None
        
        pass

    def prepareReporting(self):
        # TODO Complete Me

        rvalue = self._database.insert()
        if rvalue == -1:
            self._testcontroller.setProblem(1)
            self._reporter.prepare()
        
        pass

    def setReporter(self, reporter):
        # TODO Complete Me

        self._reporter = reporter
        
        pass

    def setDB(self, database):
        # TODO Complete Me

        self._database = database
        
        pass

    def publishReport(self):
        # TODO Complete Me

        self._database.update()
        rvalue = self._reporter.report()
        
        pass

    def setTC(self, testcontroller):
        # TODO Complete Me

        self._testcontroller = testcontroller
        
        pass


if __name__ == '__main__':
    reporter = Reporter()
    database = Database()
    testmanager = TestManager()
    testmanager.setReporter(reporter)
    testmanager.setDB(database)
    reporter.setTM(testmanager)
    database.setTM(testmanager)

    for i in range(3):
        testcontroller = TestController()
        testcontroller.setTM(testmanager)
        testmanager.setTC(testcontroller)
        testcontroller.setup()
        testcontroller.execute()
        testcontroller.tearDown()
