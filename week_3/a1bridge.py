class SistemOperasi:

    """ Target interface.
    This is the target interface, that clients use.
    """

    def anotherFunctionality(self):
        raise NotImplemented()


class UseCase:

    """ Bridge class.
    
    This class forms a bridge between the target
    interface and background implementation.
    """

    def __init__(self, sistemOperasi):
        self.sistemOperasi = sistemOperasi
    def someFunctionality(self):
        raise NotImplemented()


class LogOff(UseCase):

    """ Variant of the target interface.
    This is a variant of the target Abstract interface.
    It can do something little differently and it can
    also use various background implementations through
    the bridge.
    """
    
    def __init__(self, os):
        self.os = os

    def someFunctionality(self):
        print ("LogOff: "+ str(self.os.anotherFunctionality()))


class Shutdown(UseCase):
    def __init__(self, os):
        self.os = os

    def someFunctionality(self):
        print ("Shutdown: "+ str(self.os.anotherFunctionality()))

class Ubuntu(SistemOperasi):

    """ Concrete background implementation.
    A variant of background implementation, in this
    case for Ubuntu
    """

    def anotherFunctionality(self):
        return self.__class__.__name__

class Windows(SistemOperasi):
    def anotherFunctionality(self):
        return self.__class__.__name__


def main():
    ubuntu = Ubuntu()
    windows = Windows()

    useCase = LogOff(ubuntu)
    # [1]
    useCase.someFunctionality()
    """
    Output for code [1]
    LogOff : Ubuntu
    """

    useCase = LogOff(windows)
    # [2]
    useCase.someFunctionality()
    """
    Output for code [2]
    LogOff : Windows
    """

    useCase = Shutdown(ubuntu)
    # [3]
    useCase.someFunctionality()
    """
    Output for code [3]
    Shutdown : Ubuntu
    """
    useCase = Shutdown(windows)

    # [4]
    useCase.someFunctionality()
    """
    Output for code [4] :
    Shutdown : Windows
    """


if __name__ == "__main__":
    main()
