class Color():
    def drawCircle(self,radius, x, y):
        pass

class Shape():
    def __init__(self, color):
        self.color = color;

    def color_it(self):
        pass

class Circle(Shape):
    def __init__(self,color, radius, x, y):
        self.color = color
        self.radius = radius
        self.x = x
        self.y = y

    def color_it(self):
        self.color.drawCircle(self.radius, self.x, self.y)

class RedColor(Color):
    def drawCircle(self,radius, x, y):
        print("Drawing Circle[ color: red, radius: {} , x: {}, {}]".format(radius,x,y))

class GreenColor(Color):
    def drawCircle(self,radius, x, y):
        print("Drawing Circle[ color: green, radius: {} , x: {}, {}]".format(radius,x,y))

if __name__ == '__main__':
    s1 = Circle(RedColor(),10,100,100)
    s1.color_it()

    s2 = Circle(GreenColor(),10,100,100)
    s2.color_it()
